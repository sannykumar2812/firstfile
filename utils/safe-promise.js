function safepromise(promise){
  return promise
    .then(function(data){
      return[null,data]
    }).catch(function(err){
      return [err]
    })
}

module.exports = safepromise;