const redisconnection =require('../config/redis')

function getdata(key){
  return new Promise(function(resolve,reject){
    redisconnection.get(key,function(err,data){
      if(err){
        return reject(err);
      }
      resolve(data);
    })
  })
}

function setdata(key,value){
  redisconnection.set(key,value,function(err){
    if (err){
      // console.log(err);
    }
  })
}

module.exports= {
  get : function(key){
    return getdata(key)
  },
  set : function(key,value){
    setdata(key,value)
  }

}