// Different ways to create a promise

//1. Natural way (Generic)
function p1 (){
    return new Promise(function(resolve, reject){
        ///promisifying the non blocking code.
        //if err => reject
        // otherwise => reoslve
    });
}

//2.
async function p2(){
    try {
        const data = await promiseFn1();// this is a  function which returns a promise
        //doing some processing on the data
        const data2 = await promiseFn2();// this is a  function which returns a promise
        //doing some processing on the data2
        return {
            data,
            data2
        };
    }catch(error){
        throw error;
    }
}

function p2_2(){
    new Promise(async function(resolve, reject){
        try {
            const data = await promiseFn1();// this is a  function which returns a promise
            //doing some processing on the data
            fs.readFile('', function(err,data2){
                if(err){
                    return reject(err);
                }
                resolve({
                    data, 
                    data2
                })
            });
        }catch(e){
            reject(e);
        }
    });
}


async function p2(){
    const [error, data] = await safePomise(promiseFn1());// this is a  function which returns a promise
    //doing some processing on the data
    if(error){
        throw error;
    }
    const [error2, data2]  = await safePomise(promiseFn2());// this is a  function which returns a promise
    //doing some processing on the data2
    if(error2){
        throw error2;
    }

    return {
        data, 
        data2
    }
}


//3. using async await in callback function

async function p3(){
    
    fs.readFile('ffff',function(err, data){
        //err
        if(err){
            return Promise.reject(err);
        }
        // data
        Promise.resolve(data); 
    },20000);
    
}

async function p3_1(){
    try {
        const data = await promiseFn1(); // this is a  function which returns a promise
        //doing some processing on the data
        fs.readFile('', function(err,data2){
            if(err){
                return Promise.reject(err);
            }
            Promise.resolve({
                data,
                data2
            })
        });
    }catch(e){
        throw e;
    }
}


function fileRead(file){
    return new Promise(function(resolve, reject){
        fs.readFile(file, function(err,data2){
            if(err){
                return reject(err);
            }
            resolve(data2);
        });
    });
}


async function p3_2(){
    try {
        const data = await promiseFn1(); // this is a  function which returns a promise
        //doing some processing on the data
        const file = await fileRead('file');
        return {
            data,
            file
        }
    }catch(e){
        throw e;
    }
}


//4. using native promise from inner fun 

function p4(){
    //processing some things
    return promiseFn1();// this is a  function which returns a promise
}





1:49

//=============================== How to use, consume the promise ========================//
 
p1()
    .then(function(data){ })
    .catch(function(err){ });

(async function(){
    try {
        const data = await p1();
    }catch(e){

    }
})();

(async function(){
    const data = await p1().catch(function(error){ 
        return {
            error
        }
    })

    if(data.error){
        return 
    }
    // process the data
})();

(async function(){
    const [error, data] = await safePromise(p1());
    
    if(error){
        return 
    }
    // process the data
})();