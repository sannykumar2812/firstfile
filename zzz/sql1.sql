CREATE TABLE sanny(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    name varchar(15),
    Age INT,
	Created_at datetime,
    Updated_at datetime,
    IsActive INT DEFAULT 1
);
INSERT INTO sanny (name,age) VALUE("hemant","30");
ALTER TABLE sanny MODIFY created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE sanny MODIFY Updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE sanny MODIFY Updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;