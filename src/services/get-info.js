const {knex}=require('../../config/db');

module.exports = function(){

  return knex.select('user.id','user.name','user.age','user.email','user.gender','user_meta_info.user_id','user_meta_info.column_key','user_meta_info.column_value')
    .from('user').rightJoin('user_meta_info', 'user.id', 'user_meta_info.user_id')
    // .where(`user.id`,id)
    
}
