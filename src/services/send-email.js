const {google} =  require('googleapis');

const {gmail, auth}=google;
module.exports = async function sendEmail(payload){

    const accessToken = process.env.accessToken;
    const refreshToken = process.env.refreshToken;

    const oauth2Client = new auth.OAuth2(process.env.GOOGLE_CLIENT,process.env.GOOGLE_SECRET, process.env.GOOGLE_CALLBACK);

    oauth2Client.credentials = {
    access_token: accessToken,
    refresh_token: refreshToken
    };
    const instance = gmail({
    version: 'v1',
    auth: oauth2Client
    });

const subject = payload.subject;
const utf8Subject = `=?utf-8?B?${Buffer.from(subject).toString('base64')}?=`;
const messageParts = [
    `From: ${process.env.EMAIL}`,
    `To: ${payload.to}`,
    'Content-Type: text/html; charset=utf-8',
    'MIME-Version: 1.0',
    `Subject: ${utf8Subject}`,
    '',
    payload.message,
];
const message = messageParts.join('\n');

const encodedMessage = Buffer.from(message)
    .toString('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');

const result = await instance.users.messages.send({
    userId: 'me',
    requestBody: {
        raw: encodedMessage,
    },
});

return result;
}