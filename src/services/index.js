module.exports = {
  setInfo : require('./set-info'),
  getInfo : require('./get-info'),
  sendEmail : require('./send-email')
}
