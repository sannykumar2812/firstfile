// const async = require("async"); 


// async.parallel([                   //ARRAY
//     function(callback) {
//         console.log("1");
//         setTimeout(function() {
//             callback(null, 'one');
//         }, 2000);
//         console.log("2");
//     },
//     function(callback) {
//         console.log("3");
//         setTimeout(function() {
//             callback(null, 'two');
//         }, 3000);
//         console.log("4");
//     }
// ], function(err, results) {
//     console.log(results);
// });


// async.parallel({                                // OBJECT
//     one: function(callback) {
//         console.log("1");
//         setTimeout(function() {
//             callback(null, 1);
//         }, 200);
//         console.log("2");
//     },
//     two: function(callback) {
//         console.log("3");
//         setTimeout(function() {
//             callback(null, 2);
//         }, 100);
//         console.log("4");
//     }
// }, function(err, results) {
//     console.log(results);
// });


// async.waterfall([                         //watermfall
//     function(callback) {
//         callback(null, 'one', 'two');
//     },
//     function(arg1, arg2, callback) {
        
//         callback(null, 'three');
//     },
//     function(arg1, callback) {
       
//         callback(null, 'done');
//     }
// ], function (err, data) {
//     console.log(data)
   
// });


//   async.auto({                               //auto
//      get_data: function(callback) {
//          console.log("1");
//         setTimeout(function(){
//             callback(null, 'data', 'converted to array');
//         },1000);
        
//     },
//     make_folder: function(callback) {
//         console.log("3");
//         setTimeout(function(){
//             callback(null, 'folder');
//         },1000);
        
//     },
//     write_file: ['get_data', 'make_folder', function(results, callback) {
//         // once there is some data and the directory exists,
//         // write the data to a file in the directory
//         callback(null, 'filename');
//     }],
//     email_link: ['write_file', function(results, callback) {
//         // once the file is written let's email a link to it...
//         callback(null, {'file':results.write_file, 'email':'user@example.com', });
//     }]
// }, function(err, results) {
//     if (err) {
//         console.log('err = ', err);
//     }
//     console.log('results = ', results);
// results = {
//     get_data: ['data', 'converted to array']
//     make_folder; 'folder',
//     write_file: 'filename'
//     email_link: { file: 'filename', email: 'user@example.com' }
//    } 
// });


// async.auto({
//     one : function(callback) {
//         console.log("1");
//         setTimeout(function(){
//             callback(null,"first1")
//         },1000);
        
//     },
//     two : function(callback) {
//         console.log("2");
//         setTimeout(function(){
//             callback(null,"second1")
//         },1000);
        
//     },
//     three: function(callback) {
//         console.log("3");
//         setTimeout(function(){
//             callback(null,"third1")
//         },1000);
        
//     },
//     four:  ["one" , function(data,callback) {
//          console.log("4");
//         setTimeout(function(){
//            callback(null,"fourth1")
//         },1000);
        
//     }],
//     five : [ "two" , function(data,callback) {
//         console.log("5");
//         setTimeout(function(){
//            callback(null, "fifth1")
//         },1000);
        
//     }],
//     six : ["two",function(data,callback) {
//         console.log("6");
//         setTimeout(function(){
//            callback(null,"sixth1")
//         },1000);
        
//     }],
//     seven :[ "four","five","six",function(data,callback) {
//         console.log("7");
//         setTimeout(function(){
//             callback(null,"seventh1")
//         },1000);
        
//     }],
//     eight : ["six",function(data,callback) {
//         console.log("8");
//         setTimeout(function(){
//             callback(null,"eight1")
//         },1000);
        
//     }],
//     nine : ["three","eight",function(data,callback) {
//         console.log("9");
//         setTimeout(function(){
//             callback(null,"nineth1")
//         },1000);
        
//     }],
// },function (err,data) {
//     if(err){
//         return err
//     }
//     console.log(data)
    
// });


