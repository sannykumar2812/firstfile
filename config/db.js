const mysql = require('mysql2');
const config = require('./');

const conn = mysql.createConnection({
  host: config.DB_HOST,
  port: config.DB_PORT,
  user: config.DB_USER,
  password: config.DB_PASSWORD,
  database: config.DATABASE,
});

const knex = require('knex')({
  client: 'mysql2',
  connection: {
    host : config.DB_HOST,
    port : config.DB_PORT,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    database: config.DATABASE,
  }
});
module.exports = {
  mysql: conn,
  knex
};
