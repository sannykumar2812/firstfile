const config = {
  // DATA_DIR : process.env.DATA_DIR,
  // UPLOAD_NAME : process.env.UPLOAD_NAME,
  // ASSETS_PATH : process.env.ASSETS_PATH,
  DB_HOST : process.env.DB_HOST,
  DB_PORT : process.env.DB_PORT,
  DATABASE : process.env.DATABASE,
  DB_USER : process.env.DB_USER,
  DB_PASSWORD : process.env.DB_PASSWORD,
  MOUNT_POINT : process.env.MOUNT_POINT,
}

Object.keys(config).forEach(key => {
  if(!config[key]) {
    throw new Error(`.env ${key} is empty, please set the value before stating the applications.`)
  }
})




module.exports = config;
