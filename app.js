require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const config = require('./config');
const { routes: loadApplicationRoutes } = require('./src/controller/routes');

const passport = require('passport');

var GoogleStrategy = require('passport-google-oauth20').Strategy;

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT,
  clientSecret: process.env.GOOGLE_SECRET,
  callbackURL: "http://localhost:3000/auth/google/callback"
},
function(accessToken, refreshToken, profile, cb) {
  console.log("accessToken",accessToken)
  console.log("refreshToken",refreshToken)
  User.findOrCreate({ googleId: profile.id }, function (err, user) {
    return cb(err, user);
  });
}
));


// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
// var filereadRouter = require('./routes/fileread');
// var uploadRouter = require('./routes/upload');
// var redisRouter = require('./routes/redis');
// var mysqlRouter = require('./routes/mysql');
// var knexRouter = require('./routes/knex');
// var dbRouter = require('./routes/db');
// const { SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS } = require('constants');
var app = express();

// view engine setup

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
loadApplicationRoutes(app, { MOUNT_POINT : config.MOUNT_POINT});

const DIR = process.env.DIR
const ASSESS_PATH = process.env.ASSESS_PATH
app.use(express.static(path.join(__dirname, 'public'))); 
app.use(express.static(path.resolve(DIR,ASSESS_PATH)));   //static path
app.get('/auth/google',
  passport.authenticate('google', { scope: ['profile','https://www.googleapis.com/auth/gmail.send'] ,
    accessType: 'offline',
    prompt: 'consent',
  }));



app.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

// app.use('/', indexRouter);
// app.use('/users', usersRouter);
// app.use('/file', filereadRouter);
// app.use('/up', uploadRouter);
// app.use('/redis', redisRouter);
// app.use('/mysql', mysqlRouter);
// app.use('/knex', knexRouter);
// app.use('/db', dbRouter);
// // catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, ) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
